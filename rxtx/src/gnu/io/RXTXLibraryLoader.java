package gnu.io;

public class RXTXLibraryLoader {

    public static void load() {
        String libraryName = System.getProperty("sun.arch.data.model").equals("32")?
                "rxtxSerial32" : "rxtxSerial64";
        System.loadLibrary(libraryName);
    }

}
