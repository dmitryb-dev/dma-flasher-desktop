package com.dma.params.utils;

import com.dma.params.model.Group;
import com.dma.params.model.Param;
import com.dma.params.model.Schema;
import com.dma.params.model.Value;
import com.dma.params.model.field.Field;
import com.dma.params.model.field.NumberField;
import com.dma.params.model.field.PresetField;
import com.dma.params.model.field.StringField;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.dma.gui.schema.ParseException.tryParse;

public class SchemaParserEMAP implements SchemaParser {

    @Override
    public Schema parse(String source) {
        source = removeComments(source);
        return parseSchema(source);
    }

    public Schema parseSchema(final String source) {
        return tryParse("map", null, () -> {
            Schema schema = parseDescription(getHead(source));
            Map<String, PresetField> tables =
                    getTables(source).stream()
                            .map(table -> new Object[] {parserTableName(table), parseTable(table)})
                            .collect(Collectors.toMap(tuple -> (String) tuple[0], tuple -> (PresetField) tuple[1]));
            getPages(source).stream().map(page -> parsePage(page, tables)).forEach(group -> schema.addGroup(group));
            return schema;
        });
    }

    public Schema parseDescription(String head) {
        String[] lines = head.split("(\\r\\n|\\r|\\n)");
        Schema schema = new Schema(lines[1], lines[0]);
        String speedString = lines[2].split(":")[1];
        schema.setSpeed(Integer.parseInt(speedString));
        return schema;
    }

    public Group parsePage(String source, Map<String, PresetField> tables) {
        Group group = new Group(Regex.find(source, "(?<=(<\\S{0,400}\\s))(.+)(?=>)").get(0));
        List<String> paramStrings = Regex.find(source, "(?<=(.{0,4000}[\\r\\n])).+(?=([\\r\\n].*))");
        paramStrings.stream().map(s -> parseParam(s, tables)).forEach(param -> group.addParam(param));
        return group;
    }

    public Param parseParam(String source, Map<String, PresetField> tables) {
        return tryParse("param", source, () -> {
            String[] paramDescriptors = source.split("\\|");
            Field field = paramDescriptors[4].equals("N")?
                    parseField(paramDescriptors[2], paramDescriptors[3]) : tables.get(paramDescriptors[4].substring(1).trim()).clone();
            if (field == null) throw new RuntimeException("No table for param " + paramDescriptors[0] + " table: " + paramDescriptors[4]);
            return new Param(paramDescriptors[0], paramDescriptors[1], paramDescriptors[2],
                    field, paramDescriptors[5]);
        });
    }

    public Field parseField(String type, String boundsString) {
        Integer[] bounds = Arrays.stream(boundsString.split(":")).map(Integer::parseInt).toArray(value -> new Integer[2]);
        switch (type) {
            case "str": return new StringField("", bounds[1]);
            default: return new NumberField(bounds[0], bounds[1]);
        }
    }

    public String parserTableName(String source) {
        return Regex.find(source, "(?<=(<\\S{0,200}\\s))(.+)(?=>)").get(0);
    }
    public Field parseTable(String source) {
        return tryParse("table", source, () -> {
            PresetField preset = new PresetField();
            List<String> valueStrings = Regex.find(source, "(?<=(.*[\\r\\n])).+(?=([\\r\\n].*))");
            valueStrings.stream()
                    .map(s -> s.split("\\|"))
                    .map(strings -> new Value(strings[0], strings[1]))
                    .forEach(value -> preset.addValue(value));
            return preset;
        });
    }

    public String removeComments(String source) {
        return source.replaceAll("(\\/\\/.*)|(\\/\\*(.|[\\r\\n])*\\*\\/)", "");
    }

    public String getHead(String source) {
        return Regex.find(source, "^.*[\\r\\n]+.*[\\r\\n]+.*[\\r\\n]+.*[\\r\\n]+").get(0);
    }

    public List<String> getPages(String source) {
        return Regex.find(source, "<((P|p)(AGE|age))\\s.*>[\\S\\s]*?\\1>");
    }

    public List<String> getTables(String source) {
        return Regex.find(source, "<((T|t)(ABLE|able))\\s.*>[\\S\\s]*?\\1>");
    }
}
