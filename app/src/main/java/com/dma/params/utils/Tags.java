package com.dma.params.utils;

public class Tags {

    public  static final String
            DEVICE_KEY = "device",
            SCHEMA_DESCRIPTION_KEY = "description",
            GROUPS_ARRAY_KEY = "groups",
            SPEED_KEY = "speed",

            GROUP_NAME_KEY = "group name",
            PARAMS_ARRAY_KEY = "params",

            PARAM_NAME_KEY = "param name",
            PARAM_TYPE_KEY = "type",
            ADDRESS_KEY = "address",
            VALUES_ARRAY_KEY = "values",
            FIELD_KEY = "field",
            PARAM_DESCRIPTION_KEY = "description",
            DEFAULT_VALUE_KEY = "default",
            ID_KEY = "id",

            VALUE_KEY = "value",
            VALUE_DESCRIPTION_KEY = "description",

            MIN_KEY = "min",
            MAX_KEY = "max";

}
