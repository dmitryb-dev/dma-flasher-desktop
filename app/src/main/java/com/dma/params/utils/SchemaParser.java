package com.dma.params.utils;

import com.dma.params.model.Schema;

public interface SchemaParser {

    Schema parse(String source);

}
