package com.dma.params.utils;

import com.dma.params.model.Schema;

public interface SchemaCreator {

    String create(Schema schema);

}
