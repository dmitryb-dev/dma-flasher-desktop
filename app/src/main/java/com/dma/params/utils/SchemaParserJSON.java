package com.dma.params.utils;

import com.dma.params.model.Group;
import com.dma.params.model.Param;
import com.dma.params.model.Schema;
import com.dma.params.model.Value;
import com.dma.params.model.field.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.dma.params.utils.Tags.*;

public class SchemaParserJSON implements SchemaParser {

    @Override
    public Schema parse( String jsonString) {
        JSONObject schemaJSON = new JSONObject(jsonString);
        Schema schema = new Schema(schemaJSON.getString(DEVICE_KEY) , getDescription(schemaJSON));
        schema.setSpeed(schemaJSON.has(SPEED_KEY)? schemaJSON.getInt(SPEED_KEY) : 115200);
        JSONArray groupsJSON = (new JSONObject(jsonString)).getJSONArray(GROUPS_ARRAY_KEY);
        for (Object particularGroupJSON : groupsJSON)
            schema.addGroup(parseGroup((JSONObject) particularGroupJSON));
        return schema;
    }

    private String getDescription(JSONObject json) {
        return json.has(SCHEMA_DESCRIPTION_KEY)? json.getString(SCHEMA_DESCRIPTION_KEY) : null;
    }

    private Group parseGroup(JSONObject json) {
        Group group = new Group(json.getString(GROUP_NAME_KEY));
        if (json.has(PARAMS_ARRAY_KEY))
            for (Object paramJSON : json.getJSONArray(PARAMS_ARRAY_KEY))
                group.addParam(parseParam((JSONObject) paramJSON));
        return group;
    }

    private Param parseParam(JSONObject json) {
        if (!json.has(PARAM_NAME_KEY)) return new Param();
        String description = json.has(PARAM_DESCRIPTION_KEY)? json.getString(PARAM_DESCRIPTION_KEY) : null;
        Param param = new Param(
                json.getString(PARAM_NAME_KEY),
                json.getString(ADDRESS_KEY),
                json.getString(PARAM_TYPE_KEY),
                parseField(json),
                description
        );
        if (json.has(DEFAULT_VALUE_KEY)) {
            if (param.getField() instanceof BooleanField) {
                param.getField().setDefaultValue(json.getBoolean(DEFAULT_VALUE_KEY));
            } else if (param.getField() instanceof FloatNumberField) {
                param.getField().setDefaultValue(json.getDouble(DEFAULT_VALUE_KEY));
            } else if (param.getField() instanceof NumberField) {
                param.getField().setDefaultValue(json.getInt(DEFAULT_VALUE_KEY));
            } else if (param.getField() instanceof StringField) {
                param.getField().setDefaultValue(json.getString(DEFAULT_VALUE_KEY));
            } else if (param.getField() instanceof PresetField) {
                ((PresetField) param.getField()).setDefaultField(json.getInt(DEFAULT_VALUE_KEY));
            }
        }
        return param;
    }

    private List<FieldWithId> fields = new ArrayList<>();
    private Field searchField(String id) {
        for (FieldWithId f : fields)
            if (f.getId().equals(id)) return f.clone();
        throw new JSONException("Wrong id");
    }



    private Field parseField(JSONObject json) {
        String fieldType = json.getString(FIELD_KEY).trim().toLowerCase();
        if (fieldType.charAt(0) == '#')
            return searchField(fieldType.substring(1));

        Field f = getFieldByType(fieldType, json);
        if (f instanceof FieldWithId) {
            if (json.has(ID_KEY)) {
                ((FieldWithId) f).setId(json.getString(ID_KEY));
                fields.add((FieldWithId) f);
            }
        }
        return f;
    }

    private Field getFieldByType(String type, JSONObject json) {
        switch (type) {
            case "preset":
                PresetField preset = new PresetField();
                for (Object valueJSON : json.getJSONArray(VALUES_ARRAY_KEY))
                    preset.addValue(parseValue((JSONObject) valueJSON));
                if (json.has(DEFAULT_VALUE_KEY)) preset.setDefaultField(json.getInt(DEFAULT_VALUE_KEY));
                return preset;
            case "boolean":
                return new BooleanField(json.has(DEFAULT_VALUE_KEY)? json.getBoolean(DEFAULT_VALUE_KEY) : false);
            case "number":
                long min = json.has(MIN_KEY)? json.getLong(MIN_KEY) : Integer.MIN_VALUE;
                long max = json.has(MAX_KEY)? json.getLong(MAX_KEY) : Integer.MAX_VALUE;
                return json.has(DEFAULT_VALUE_KEY)?
                        new NumberField(min, max, json.getInt(DEFAULT_VALUE_KEY)) :
                        new NumberField(min, max);
            case "float":
                double floatMin = json.has(MIN_KEY)? json.getDouble(MIN_KEY) : Float.MIN_VALUE;
                double floatMax = json.has(MAX_KEY)? json.getDouble(MAX_KEY) : Float.MAX_VALUE;
                return json.has(DEFAULT_VALUE_KEY)?
                        new FloatNumberField(floatMin, floatMax, json.getDouble(DEFAULT_VALUE_KEY)) :
                        new FloatNumberField(floatMin, floatMax);
            default:
                String defaultValue = json.has(DEFAULT_VALUE_KEY)? json.getString(DEFAULT_VALUE_KEY) : "";
                int maxLength = json.has(MAX_KEY)? json.getInt(MAX_KEY) : 256;
                return new StringField(defaultValue, maxLength);
        }
    }

    private Value parseValue(JSONObject json) {
        String value = json.getString(VALUE_KEY);
        String description = json.has(VALUE_DESCRIPTION_KEY)?
                json.getString(VALUE_DESCRIPTION_KEY) : value;
        return new Value(description, value);
    }

}
