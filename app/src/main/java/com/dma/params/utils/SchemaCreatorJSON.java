package com.dma.params.utils;

import com.dma.params.dao.SchemaDAOFile;
import com.dma.params.model.Group;
import com.dma.params.model.Param;
import com.dma.params.model.Schema;
import com.dma.params.model.Value;
import com.dma.params.model.field.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.dma.params.utils.Tags.*;

public class SchemaCreatorJSON implements SchemaCreator {
    @Override
    public String create(Schema schema) {
        JSONObject json = new JSONObject();
        json.put(DEVICE_KEY, schema.getDevice());
        if (schema.getDescription() != null) json.put(SCHEMA_DESCRIPTION_KEY, schema.getDescription());
        json.put(SPEED_KEY, schema.getSpeed());
        JSONArray array = new JSONArray();
        for (Group g : schema) {
            array.put(createGroup(g));
        }
        json.put(GROUPS_ARRAY_KEY, array);
        return json.toString(2);
    }

    private JSONObject createGroup(Group g) {
        JSONObject json = new JSONObject();

        json.put(GROUP_NAME_KEY, g.getName());
        JSONArray array = new JSONArray();
        for (Param p : g)
            array.put(createParam(p));
        json.put(PARAMS_ARRAY_KEY, array);

        return json;
    }

    private List<String> fieldIDs = new ArrayList<>();

    private FieldSaver fieldSaver = new FieldSaver();
    private JSONObject createParam(Param p) {
        JSONObject json = new JSONObject();

        if (p.getName() == null) return json;

        json.put(PARAM_NAME_KEY, p.getName());
        if (p.getDescription() != null) json.put(PARAM_DESCRIPTION_KEY, p.getDescription());
        json.put(ADDRESS_KEY, p.getAddress());
        json.put(PARAM_TYPE_KEY, p.getType());
        json.put(DEFAULT_VALUE_KEY, p.getField().getDefaultValue());

        fieldSaver.setJson(json);
        if (p.getField() instanceof FieldWithId) {
            String id = ((FieldWithId) p.getField()).getId();
            if (id != null && !id.equals(""))
                if (fieldIDs.contains(id)) {
                    json.put(FIELD_KEY, "#" + id);
                    return json;
                } else {
                    json.put(ID_KEY, id);
                    fieldIDs.add(id);
                }
        }
        p.getField().accept(fieldSaver);
        return json;
    }

    @Test
    public void test() {
        try {
            Schema s = new SchemaDAOFile(
                    new File("C:\\Users\\Дима\\IdeaProjects\\DMA device customizer\\src\\sample3.json"),
                    new SchemaParserJSON())
                    .getSchema();
            System.out.println(create(s));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private class FieldSaver implements FieldVisitor {

        private JSONObject json;

        public void setJson(JSONObject json) {
            this.json = json;
        }

        @Override
        public Object visit(BooleanField booleanField) {
            json.put(FIELD_KEY, "boolean");
            return null;
        }

        @Override
        public Object visit(FloatNumberField floatNumberField) {
            json.put(FIELD_KEY, "float");
            json.put(MIN_KEY, floatNumberField.getMin());
            json.put(MAX_KEY, floatNumberField.getMax());
            return null;
        }

        @Override
        public Object visit(NumberField numberField) {
            json.put(FIELD_KEY, "number");
            json.put(MIN_KEY, numberField.getMin());
            json.put(MAX_KEY, numberField.getMax());
            return null;
        }

        @Override
        public Object visit(PresetField presetField) {
            JSONArray array = new JSONArray();
            json.put(FIELD_KEY, "preset");
            for (Value v : presetField) {
                JSONObject valueJSON = new JSONObject();
                valueJSON.put(VALUE_KEY, v.getValue());
                valueJSON.put(VALUE_DESCRIPTION_KEY, v.getDescription());
                array.put(valueJSON);
            }
            json.put(VALUES_ARRAY_KEY, array);
            return null;
        }

        @Override
        public Object visit(StringField stringField) {
            json.put(FIELD_KEY, "string");
            json.put(MAX_KEY, stringField.getMaxLength());
            return null;
        }
    }

}
