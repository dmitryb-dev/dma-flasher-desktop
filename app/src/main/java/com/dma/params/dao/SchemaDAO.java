package com.dma.params.dao;

import com.dma.params.model.Schema;

public interface SchemaDAO {
    
    Schema getSchema() throws Exception;
    
}
