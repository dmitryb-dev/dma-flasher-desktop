package com.dma.params.dao;

import com.dma.params.model.Schema;

import java.io.IOException;

public interface SchemasListDAO extends Iterable<String> {

    Schema get(int index) throws Exception;

}
