package com.dma.params.dao;

import com.dma.params.model.Schema;
import com.dma.params.utils.Regex;
import com.dma.params.utils.SchemaParser;
import com.dma.params.utils.SchemaParserEMAP;
import com.dma.params.utils.SchemaParserJSON;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class SchemaDAOFile implements SchemaDAO{

    private File file;
    private SchemaParser parser;

    public SchemaDAOFile( File file,  SchemaParser parser) {
        this.file = file;
        this.parser = parser;
    }

    public SchemaDAOFile( File file) {
        SchemaParser parser = null;
        switch (Regex.find(file.getName(), "\\.[^.\\n\\s]+$").get(0)) {
            case ".dmaset": parser = new SchemaParserJSON(); break;
            case ".emap": parser = new SchemaParserEMAP(); break;
        }
        this.file = file;
        this.parser = parser;
    }

    @Override
    public Schema getSchema() throws IOException {
        String readFile = new String(Files.readAllBytes(file.toPath()), "windows-1251");
        return parser.parse(readFile);
    }

    public static final String[] SUPPORTED_EXTENSIONS = {
            "dmaset", "emap"
    };

    public static boolean isSettings(File file) {
        return isBelongsToFormats(file, "dmaset");
    }

    public static boolean isSupportedFormat(File file) {
        return isBelongsToFormats(file, SUPPORTED_EXTENSIONS);
    }

    public static boolean isBelongsToFormats(File file, String... formats) {
        if (!file.isFile()) return false;

        int indexOfDot = file.getName().lastIndexOf(".");
        if (indexOfDot > 0) {
            String fileExtension = file.getName().substring(indexOfDot + 1);
            for (String supportedExtension : formats)
                if (fileExtension.equals(supportedExtension)) return true;
        }
        return false;
    }
}
