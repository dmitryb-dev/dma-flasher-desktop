package com.dma.params.dao;

import com.dma.params.model.Schema;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class SchemasListDAODirectory implements SchemasListDAO {

    private List<File> files = new ArrayList<>();

    public SchemasListDAODirectory(File directory) {
        this.directory = directory;
        refresh();
    }

    @Override
    public Schema get(int index) throws Exception {
        return new SchemaDAOFile(files.get(index)).getSchema();
    }

    @Override
    public Iterator<String> iterator() {
        return getFileNames().iterator();
    }

    public Collection<String> getFileNames() {
        Collection<String> fileNames = new ArrayList<>(files.size());
        files.forEach(file -> fileNames.add(file.getName()));
        return fileNames;
    }

    public void refresh() {
        files.clear();
        if (!directory.isDirectory()) directory = directory.getParentFile();
        for (File f : directory.listFiles(pathname -> SchemaDAOFile.isSettings(pathname)))
            files.add(f);
    }

    public int size() { return files.size(); }

    public String getName(int index) {
        return getFileName(files.get(index));
    }
    public File getFile(int index) { return files.get(index); }

    private String getFileName(File file) {
        String name = file.getName();
        int lastIndexOfDot = name.lastIndexOf(".");
        if (lastIndexOfDot > 0)
            name = name.substring(0, lastIndexOfDot);
        return name;
    }

    private File directory;
}
