package com.dma.params.model.field;

public interface Field {
    Field clone();
    Object accept(FieldVisitor visitor);
    Object getDefaultValue();
    void setDefaultValue(Object value);
}
