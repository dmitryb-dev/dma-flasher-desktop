package com.dma.params.model;

import com.dma.params.model.field.Field;
import com.dma.params.model.field.FieldWithId;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Schema implements Iterable<Group>, Cloneable {

    public Schema clone() {
        Schema clone = new Schema(device, description);
        for (Group g : groups)
            clone.addGroup(g.clone());
        return clone;
    }

    private List<Group> groups = new ArrayList<>();

    private String description;
    private String device;

    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    private int speed;

    public Schema(String device) {
        this.device = device;
    }
    public Schema(String device, String description) {
        this(device);
        this.description = description;
    }


    public String getDevice() {
        return device;
    }
    public void setDevice(String device) {
        this.device = device;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addGroup(Group group) {
        groups.add(group);
    }
    public boolean removeGroup(Group group) {
        return groups.remove(group);
    }
    public int size() {
        return groups.size();
    }

    @Override
    public Iterator<Group> iterator() {
        return groups.iterator();
    }

    public Group getGroup(int index) {
        return groups.get(index);
    }

    public Field getFieldById(String id) {
        for (Group g : this)
            for (Param p : g)
                if (p.getField() instanceof FieldWithId)
                    if (((FieldWithId) p.getField()).getId().equals(id)) return p.getField();
        return null;
    }
}
