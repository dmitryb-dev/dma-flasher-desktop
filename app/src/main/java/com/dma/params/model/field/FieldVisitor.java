package com.dma.params.model.field;

public interface FieldVisitor {

    default Object visit(Field field) {
        return null;
    }
    default Object visit(BooleanField booleanField) {
        return null;
    }
    default Object visit(FloatNumberField floatNumberField) {
        return null;
    }
    default Object visit(NumberField numberField) {
        return null;
    }
    default Object visit(PresetField presetField) {
        return null;
    }
    default Object visit(StringField stringField) {
        return null;
    }

}
