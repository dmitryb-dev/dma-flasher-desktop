package com.dma.params.model.field;

public abstract class FieldWithId implements Field {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Field clone() {
        return null;
    }
}
