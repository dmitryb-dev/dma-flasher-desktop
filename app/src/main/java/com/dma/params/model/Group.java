package com.dma.params.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Group implements Iterable<Param> {

    private String name;
    private Collection<Param> params = new ArrayList<>();

    public void addParam(Param p) {
        params.add(p);
    }
    public void removeParam(Param p) {
        params.remove(p);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group(String name) {
        this.name = name;
    }

    @Override
    public Iterator<Param> iterator() {
        return params.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group params = (Group) o;

        return name.equals(params.name);
    }

    public Group clone() {
        Group clone = new Group(name);
        for (Param p : params)
            clone.addParam(p.clone());
        return clone;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
