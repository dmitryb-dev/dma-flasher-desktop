package com.dma.params.model;

import com.dma.params.model.field.Field;
import com.dma.params.model.field.FieldWithId;

public class Param implements Cloneable {

    public Param clone() {
        Field clone = null;
        if (field != null) {
            clone = field.clone();
            if (field instanceof FieldWithId) {
                ((FieldWithId) clone).setId(((FieldWithId) field).getId());
            }
        }
        return new Param(name, address, type, clone, description);
    }

    private String name;
    private String address;
    private Field field;
    private String type;
    private String description;

    public Param() {
    }

    public Param(String name, String address, String type) {
        this.name = name;
        this.address = address;
        this.type = type;
    }

    public Param(String name, String address, String type, Field field) {
        this(name, address, type);
        this.field = field;
    }

    public Param(String name, String address, String type, String description) {
        this(name, address, type);
        this.description = description;
    }
    public Param(String name, String address, String type, Field field, String description) {
        this(name, address, type);
        this.field = field;
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Field getField() { return field; }

    public String getDescription() {
        return description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Param values = (Param) o;

        if (!name.equals(values.name)) return false;
        if (!address.equals(values.address)) return false;
        return type == values.type;
    }


}
