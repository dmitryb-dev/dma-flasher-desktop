package com.dma.params.model.field;

public class NumberField extends FieldWithId {

    private long min, max, defaultValue;

    public long getMin() {
        return min;
    }

    public long getMax() {
        return max;
    }

    public NumberField() {
        max = Integer.MAX_VALUE;
        min = Integer.MIN_VALUE;
        defaultValue = 0;
    }

    public NumberField(long min, long max) {
        this.min = min;
        this.max = max;

        if (min <= 0 && max >= 0) defaultValue = 0L;
        else defaultValue =  min > 0? min : max;
    }

    public NumberField(long min, long max, long defaultValue) {
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;
    }

    @Override
    public NumberField clone() {
        return new NumberField(min, max, defaultValue);
    }

    @Override
    public Object accept(FieldVisitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public Long getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(Object value) {
        defaultValue = (int) value;
    }
}
