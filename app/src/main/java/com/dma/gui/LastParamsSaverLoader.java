package com.dma.gui;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.prefs.Preferences;

public class LastParamsSaverLoader {

    public LastParamsSaverLoader() {
        preferences = Preferences.userNodeForPackage(this.getClass());
    }

    private Preferences preferences;
    private static final String LAST_OPENED_KEY = "LAST_OPENED_SCHEMA";
    private static final String LAST_OPENED_FILES_KEY = "LAST_OPENED_FILES";
    private static final String DEFAULT_PATH = "/SETTINGS/default.emap";
    private static final String LAST_SELECTED_COMM = "LAST_SELECTED_COMM";

    public void saveSelectedComm(String commName) {
        preferences.put(LAST_SELECTED_COMM, commName);
    }
    public String getSelectedComm() {
        return preferences.get(LAST_SELECTED_COMM, null);
    }

    public void saveLastFile(File f) {
        preferences.put(LAST_OPENED_KEY, f.getAbsolutePath());
    }

    public File loadLastFile() {
        String path = preferences.get(LAST_OPENED_KEY, getAppDirectory(this) + DEFAULT_PATH);
        File file = new File(path);
        return file;
    }

    public void saveLastFilesDirectory(File directory) {
        preferences.put(LAST_OPENED_FILES_KEY, directory.getAbsolutePath());
    }

    public File loadLastDirectory() {
        String path = preferences.get(LAST_OPENED_FILES_KEY, null);
        if (path == null) path = getAppDirectory(this) + "/SETTINGS/";
        return path != null? new File(path) : getAppDirectory(this);
    }


    public String getSETTINGSDirectory() {
        return getAppDirectory(this) + "/SETTING/";
    }
    public String getMAPDirectory() {
        return getAppDirectory(this) + "/MAP/";
    }

    public static File getAppDirectory(Object o) {
        String path = o.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        try {
            path = URLDecoder.decode(path, "utf-8");
        } catch (UnsupportedEncodingException e) {
            path = System.getProperty("user.home");
        }
        File file = new File(path);
        if (!file.isDirectory()) file = file.getParentFile();
        return file;
    }

}
