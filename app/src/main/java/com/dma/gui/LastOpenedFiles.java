package com.dma.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

public class LastOpenedFiles {
    public void add(File f) {
        if (!f.exists()) return;
        files.add(0, f);
        if (files.size() > MAX_LENGTH) files.remove(files.size() - 1);
        preferences.putByteArray(PREFERENCES_KEY, Utils.objectToBytes(files));
    }

    public List<File> get() {
        List<File> files = (List<File>) Utils.bytesToObject(preferences.getByteArray(PREFERENCES_KEY, null));
        if (files == null) files = new ArrayList();
        this.files = files.stream().filter(File::exists).collect(Collectors.toList());
        return this.files;
    }

    public File get(int index) {
        return files.get(index);
    }

    private List<File> files = new ArrayList<>();
    private int MAX_LENGTH = 15;
    private Preferences preferences = Preferences.userNodeForPackage(this.getClass());
    private static String PREFERENCES_KEY = "LAST_OPENED_FILES_LIST";
}
