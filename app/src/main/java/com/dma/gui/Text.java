package com.dma.gui;

public class Text {

    public final static String
        EXIT_REALLY = "Settings were changed but not saved on the device.\n" +
            "Do you really want to exit?",

        SCHEMA_NOT_OPENED = "Map not opened. Select \"File\" -> \"Open schema\"",
        COM_NOT_SELECTED = "COM port not selected. Select \"COM ports\" -> <Your COM>";

    public final static String
        EXIT_TITLE = "Are you sure?";

    public final static String
        FILE_DIALOG_TITLE = "Select a map file",
        SETTINGS_FILE_DIALOG_TITLE = "Select a settings file",
        DIRECTORY_DIALOG_TITLE = "Select one file from the target directory";

    public final static String
        DEVICE_DOES_NOT_RESPOND = "Device doesn't respond, please check connection",
        SOMETHING_BAD = "Something bad happened";

}
