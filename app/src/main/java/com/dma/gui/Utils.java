package com.dma.gui;

import java.io.*;

public class Utils {
    public static byte[] objectToBytes(Object o) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            ObjectOutput objectOutput = new ObjectOutputStream(stream);
            objectOutput.writeObject(o);
            objectOutput.flush();
            return stream.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }
    public static Object bytesToObject(byte[] array) {
        if (array == null) return null;
        ByteArrayInputStream stream = new ByteArrayInputStream(array);
        try {
            ObjectInput objectInput = new ObjectInputStream(stream);
            return objectInput.readObject();
        } catch (Exception e) {
            return null;
        }
    }
}
