package com.dma.gui;

import com.dma.connection.COMConnection;
import com.dma.connection.DMANewProtocol;
import com.dma.connection.Logs.ConnectionLogger;
import com.dma.connection.Logs.ConsoleLogger;
import com.dma.connection.exceptions.DeviceException;
import com.dma.gui.errors.*;
import com.dma.gui.schema.SchemaChooser;
import com.dma.gui.schema.SchemaView;
import com.dma.params.dao.SchemaDAOFile;
import com.dma.params.dao.SchemasListDAODirectory;
import com.dma.params.model.Param;
import com.dma.params.model.Schema;
import com.dma.params.utils.SchemaCreatorJSON;
import gnu.io.CommPortIdentifier;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.dma.gui.COMDeviceUtils.getPortsList;
import static com.dma.gui.Text.EXIT_REALLY;
import static com.dma.gui.Text.EXIT_TITLE;

public class Controller {

    private MenuItem commMock = new MenuItem("No comm found");

    public Controller() {}
    public Controller(TabPane tabs, Menu schemasMenu, TitledPane title) {
        this.tabs = tabs;
        this.schemasMenu = schemasMenu;
        this.title = title;
    }

    @FXML private TabPane tabs;
    @FXML private Menu schemasMenu;
    @FXML private Menu comPortsMenu;
    @FXML private TitledPane title;
    @FXML private TextArea description;
    @FXML private ProgressBar progress;

    @FXML private Button sendButton;
    @FXML private Button readButton;
    @FXML
    public void initialize() {
        //    loadLastSchema();
        //   loadLastFiles();
        showLastFiles();
        ConnectionLogger.enable(true);
        ConnectionLogger.addLogger(new ConsoleLogger());
        boolean libraryLoaded = new LoadLibraryHandler().execute(() -> {
            selectLastComm();
            commMenuCrutch();
            return true;
        });
        if (!libraryLoaded) {
            comPortsMenu.setDisable(true);
            sendButton.setDisable(true);
            readButton.setDisable(true);
        }
    }

    private AtomicBoolean devourMenuShown = new AtomicBoolean(false);
    private void commMenuCrutch() {
        comPortsMenu.getItems().add(commMock);
        comPortsMenu.setOnShowing((event) -> {
            if (devourMenuShown.get()) return;
            List<CommPortIdentifier> ports = getPortsList();
            comPortsMenu.hide();
            if (ports.size() > 0)
                showComPortsInMenu(ports);
            else {
                comPortsMenu.getItems().clear();
                comPortsMenu.getItems().add(commMock);
            }
            devourMenuShown.set(true);
            comPortsMenu.show();
            devourMenuShown.set(false);
        });
    }

    private LastParamsSaverLoader paramsLoader = new LastParamsSaverLoader();
    private void loadLastSchema() {
        File file = paramsLoader.loadLastFile();
        if (file != null) {
            try {
                Schema schema = new SchemaDAOFile(file).getSchema();
                showSchema(schema, file.getName());
            } catch (Exception e) {}
        }
    }

    private void loadLastFiles() {
     /*   File directory = paramsLoader.loadLastDirectory();
        if (directory != null) {
            loadedSchemas = new SchemasListDAODirectory(directory);
            showFilesInMenu(loadedSchemas.getFileNames());
        }
        */

    }

    private void selectLastComm() {
        String commName = paramsLoader.getSelectedComm();
        if (commName == null) return;
        getPortsList().stream()
                .filter(comm -> commName.equals(comm.getName())).findFirst()
                .ifPresent(comm -> selectedComm = comm);
    }

    public void onExit(ActionEvent actionEvent) {
        if (connection != null) connection.close();
        if (!isSomethingChanged() || onExitDialog()) {
            Platform.exit();
            System.exit(0);
        }
    }

    public boolean onExitDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, EXIT_REALLY, ButtonType.YES, ButtonType.NO);
        alert.setHeaderText(null);
        alert.setTitle(EXIT_TITLE);
        ((Button) alert.getDialogPane().lookupButton(ButtonType.YES)).setDefaultButton(false);
        ((Button) alert.getDialogPane().lookupButton(ButtonType.NO)).setDefaultButton(true);
        return alert.showAndWait().get() == ButtonType.YES;
    }

    private Schema openedSchema;
    public Schema getOpenedSchema() { return openedSchema; }
    private SchemaView schemaView;

    public void showSchema(Schema schema, String fileName) {
        openedSchema = schema;
        title.setText(schema.getDescription() + " : Type Device: " + schema.getDevice() + "      File name: " + fileName);
        (schemaView = new SchemaView(tabs, description)).show(schema);
        if (connection != null && openedSchema.getSpeed() != connection.getSpeed())
            connect(connection.getIdentifier(), openedSchema.getSpeed());
    }

    public void showSchema(File file) {
        Schema schema = readSchema(file);
        if (schema == null) return;
        if (file.getName().endsWith(".emap")) {
            readRequired = true;
        }
        showSchema(schema, file.getName());
        paramsLoader.saveLastFile(file);
    }

    private boolean readRequired = false;
    public void onOpen(ActionEvent actionEvent) {
        File file = new SchemaChooser(getWindow()).getFileByDialog();
        if (file == null) return;
        showSchema(file);
    }

    public void onLoadSettings(ActionEvent actionEvent) {
        File file = new SchemaChooser(getWindow()).getSettingsFileByDialog();
        if (file == null) return;
        lastOpenedFiles.add(file);
        showLastFiles();
        showSchema(file);
    }

    public void onSchemaSelect(int number) {
        Schema schema = new OpenSchemaHandler().execute(() -> loadedSchemas.get(number));
        if (schema == null) return;
        showSchema(schema, loadedSchemas.getName(number));
        paramsLoader.saveLastFile(loadedSchemas.getFile(number));
    }

    public void onLoad(ActionEvent actionEvent) {
 /*       File directory = new SchemaChooser(getWindow()).getDirectoryByDialog();
        if (directory != null) {
            loadedSchemas = new SchemasListDAODirectory(directory);
            showFilesInMenu(loadedSchemas.getFileNames());
            paramsLoader.saveLastFilesDirectory(directory);
        }*/
    }

    private void showFilesInMenu(Collection<String> fileNames) {
        if (fileNames.size() <= 0) {
            schemasMenu.setDisable(true);
            return;
        }

        schemasMenu.getItems().clear();
        int i = 0;
        for (String fileName : fileNames) {
            MenuItem menuItem = new MenuItem(fileName);
            final int number = i++;
            menuItem.setOnAction(event -> onSchemaSelect(number));
            schemasMenu.getItems().add(menuItem);
        }
        schemasMenu.setDisable(false);
    }

    private void showLastFiles() {
        List<File> files = lastOpenedFiles.get();
        schemasMenu.getItems().clear();
        schemasMenu.setDisable(files.size() == 0);
        for (File file : files) {
            MenuItem item = new MenuItem(file.getName());
            item.setOnAction((event) -> {
                if (file.exists())
                    showSchema(file);
                else {
                    Messages.showError("No such file");
                    showLastFiles();
                }
            });
            schemasMenu.getItems().add(item);
        }
    }
    private LastOpenedFiles lastOpenedFiles = new LastOpenedFiles();


    private void showComPortsInMenu(Collection<CommPortIdentifier> ports) {
        comPortsMenu.getItems().clear();
        for (CommPortIdentifier port : ports) {
            CheckMenuItem menuItem = new CheckMenuItem(port.getName());
            menuItem.setSelected(selectedComm != null && port.getName().equals(selectedComm.getName()));
            menuItem.setOnAction(event -> {
                onPortSelect(port);
                //    showComPortsInMenu(getPortsList());
            });
            comPortsMenu.getItems().add(menuItem);
        }
    }

    private boolean checkSchemaCorrespondsToDevice() {
        return checkDevice(openedSchema.getDevice());
    }

    private String getDeviceName() {
        return new DeviceCheckHandler().execute(() -> protocol.getDeviceName());
    }
    private String getDeviceVersion() {
        return new DeviceCheckHandler().execute(() -> protocol.getDeviceVersion());
    }

    @FXML Label deviceStatus;
    private void updateDeviceStatus(String deviceName) {
        if (deviceName != null) {
            deviceStatus.setText("Device connected: " + deviceName);
            deviceStatus.setStyle("-fx-font-size: 13; -fx-text-fill: green");
        }
        else {
            deviceStatus.setText("Device disconnected");
            deviceStatus.setStyle("-fx-font-size: 13; -fx-text-fill: red");
        }
    }

    private boolean checkDevice(String requiredName) {
        String deviceName = getDeviceName();
        Platform.runLater(() -> updateDeviceStatus(deviceName));
        if (deviceName == null) return false;
        if (requiredName.trim().equals(deviceName.trim())) return true;
        Messages.showError("Wrong device. You have opened schema for " + requiredName + ", so you trying to send it for " + deviceName);
        return false;
    }

    public void writeDevice(ActionEvent actionEvent) {
        if (readRequired && !Messages.ask("Device data hasn't been read. Are you sure?")) {
            return;
        }
        readWriteDeviceAction(() -> {
            if (!checkSchemaCorrespondsToDevice()) return;
            readRequired = false;
            DeviceCommunicationHandler handler = new DeviceCommunicationHandler();
            Set<Map.Entry<Param, Object>> values = schemaView.getValues().entrySet();
            int i = 1;
            for (Map.Entry<Param, Object> pair : values) {
                progress.setProgress((double) i++ / values.size());
                String res = handler.execute(() -> {
                    protocol.write(pair.getKey(), pair.getValue());
                    return "success";
                });
                if (res == null) {
                    progress.setProgress(0);
                    return;
                }
                //   if (res == null) return; // End after first mistake.
            }
        });
    }

    public void readDevice(ActionEvent actionEvent) throws DeviceException {
        readWriteDeviceAction(() -> {
            if (!checkSchemaCorrespondsToDevice()) return;
            readRequired = false;
            DeviceCommunicationHandler handler = new DeviceCommunicationHandler();
            Set<Map.Entry<Param, Object>> values = schemaView.getValues().entrySet();
            int i = 1;
            for (Map.Entry<Param, Object> pair : values) {
                progress.setProgress((double) i++ / values.size());
                String read = handler.execute(() -> protocol.read(pair.getKey()));
                if (read != null) {
                    Platform.runLater(() -> schemaView.setParam(pair.getKey(), read));
                } else {
                    progress.setProgress(0);
                    return;
                }
            }
        });
    }
    private void readWriteDeviceAction(Runnable action) {
        if (openedSchema == null) {
            Messages.showError("Map file isn't opened");
            return;
        }
        if (deviceLocked.get()) {
            Messages.showInfo("Device is busy", "Device is busy, please wait.");
            return;
        }
        try {
            deviceLocked.set(true);
            refreshSelectedComm();
            if (!connect(selectedComm, openedSchema.getSpeed())) {
                deviceLocked.set(false);
                return;
            }
            writeReadExecutor.execute(() -> {
                try {
                    action.run();
                    disconnect();
                } finally {
                    deviceLocked.set(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            deviceLocked.set(false);
        }
    }
    private AtomicBoolean deviceLocked = new AtomicBoolean(false);
    private ExecutorService writeReadExecutor = Executors.newSingleThreadExecutor();


    private boolean connect(CommPortIdentifier identifier, int speed) {
        if (connection != null) connection.close();
        connection = new DeviceConnectionHandler().execute(() -> new COMConnection(identifier, speed));
        if (connection == null) {
            protocol = null;
            return false;
        }
        protocol = new DMANewProtocol(connection);
        return true;
    }
    private void disconnect() {
        if (connection != null) connection.close();
        connection = null;
        protocol = null;
    }
    private void refreshSelectedComm() {
        if (selectedComm == null) return;
        getPortsList().stream()
                .filter(commPortIdentifier -> commPortIdentifier.getName().equals(selectedComm.getName()))
                .findFirst()
                .ifPresent(commPortIdentifier -> selectedComm = commPortIdentifier);
    }

    private CommPortIdentifier selectedComm;
    private DMANewProtocol protocol;
    private COMConnection connection;
    private void onPortSelect(CommPortIdentifier identifier) {
        selectedComm = identifier;
        paramsLoader.saveSelectedComm(identifier.getName());
        //    onDeviceInfo(null);
    }

    private Schema readSchema(File file) {
        SchemaDAOFile dao = new SchemaDAOFile(file);
        OpenSchemaHandler handler = new OpenSchemaHandler();
        return handler.execute(() -> dao.getSchema());
    }

    private SchemasListDAODirectory loadedSchemas;

    private boolean isSomethingChanged() {
        return false;
    }

    private Window getWindow() {
        return tabs.getScene().getWindow();
    }

    public void onSave(ActionEvent actionEvent) {
        try {
            if (schemaView == null || schemaView.getSchema() == null) {
                Messages.showError("No file opened.");
                return;
            }
            File file = new SchemaChooser(getWindow()).getFileBySaveDialog();
            if (file == null) return;
            String res = new SchemaCreatorJSON().create(schemaView.getSchema());
            Files.write(file.toPath(), res.getBytes("windows-1251"));
            // loadedSchemas.refresh();
            //    showFilesInMenu(loadedSchemas.getFileNames());
            lastOpenedFiles.add(file);
            showLastFiles();
        } catch (Exception e) {
            Messages.showError(e, "Can't save file.");
        }
    }

    public void onDeviceInfo(ActionEvent actionEvent) {
        if (deviceLocked.get()) {
            Messages.showInfo("Device is busy", "Device is busy now, please wait.");
            return;
        }
        try {
            deviceLocked.set(true);
            refreshSelectedComm();
            int speed = openedSchema == null? 115200 : openedSchema.getSpeed();
            if (!connect(selectedComm, speed)) {
                deviceLocked.set(false);
                return;
            }
            writeReadExecutor.execute(() -> {
                try {
                    String deviceName = getDeviceName();
                    String deviceVersion = getDeviceVersion();
                    if (deviceName != null && deviceVersion != null)
                        Platform.runLater(() -> Messages.showInfo("Device info", "Device name: " + deviceName + "\nDevice version: " + deviceVersion));
                    disconnect();
                } finally {
                    deviceLocked.set(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            deviceLocked.set(false);
        }
    }

    public void onLogWindowOpen(ActionEvent actionEvent) {
        try {
            new LogWindow();
        } catch (IOException e) {
            Messages.showError(e, "Can't open logs");
        }
    }


}
