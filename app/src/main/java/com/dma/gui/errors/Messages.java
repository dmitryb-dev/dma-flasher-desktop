package com.dma.gui.errors;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Messages {

    public static void showError(String text) {
        Platform.runLater(() -> {
            Alert a = new Alert(Alert.AlertType.ERROR, text);
            a.setHeaderText(null);
            a.setTitle("Error");
            a.showAndWait();
        });
    }

    public static void showError(Exception ex, String text) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR, text);
            alert.setHeaderText(null);
            alert.setTitle("Error");

            if (ex != null) {
                alert.getDialogPane().setExpandableContent(getExceptionStacktraceView(ex));
            }
            alert.showAndWait();
        });
    }

    public static void showInfo(String title, String text) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle(title);
            alert.setContentText(text);
            alert.showAndWait();
        });
    }


    private static Pane getExceptionStacktraceView(Exception ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exception = sw.toString();

        Label label = new Label("The exception stacktrace:");

        TextArea textArea = new TextArea(exception);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        return expContent;
    }

    public static boolean ask(String text) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setContentText(text);
        return alert.showAndWait().get() == ButtonType.OK;
    }
}
