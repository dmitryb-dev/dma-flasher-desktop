package com.dma.gui.errors;

import com.dma.gui.Text;
import com.dma.params.model.Schema;

import java.io.IOException;

public class OpenSchemaHandler implements ErrorHandler<Schema> {

    @Override
    public Schema execute(Action<Schema> a) {
        try {
            return a.action();
        } catch (IOException e) {
            Messages.showError(e, "Can't read your file");
        } catch (RuntimeException e) {
            Messages.showError(e, "Error in map description");
        } catch (Exception e) {
            Messages.showError(e, Text.SOMETHING_BAD);
        }
        return null;
    }

}
