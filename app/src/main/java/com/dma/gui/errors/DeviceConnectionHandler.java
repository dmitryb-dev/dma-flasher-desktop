package com.dma.gui.errors;

import com.dma.connection.COMConnection;
import com.dma.gui.Text;

import static com.dma.gui.Text.COM_NOT_SELECTED;

public class DeviceConnectionHandler implements ErrorHandler<COMConnection> {
    @Override
    public COMConnection execute(Action<COMConnection> a) {
        try {
            return a.action();
        } catch (NullPointerException e) {
            Messages.showError(COM_NOT_SELECTED);
        } catch (Exception e) {
            Messages.showError(e, Text.DEVICE_DOES_NOT_RESPOND);
        }
        return null;
    }
}
