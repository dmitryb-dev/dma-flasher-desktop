package com.dma.gui.errors;

public interface ErrorHandler<T> {

    T execute(Action<T> a);

}
