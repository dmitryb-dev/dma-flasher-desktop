package com.dma.gui.errors;

public interface Action<T> {

    T action() throws Exception;

}
