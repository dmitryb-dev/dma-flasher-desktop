package com.dma.gui.errors;

import com.dma.connection.exceptions.DeviceDisconnectedException;
import com.dma.connection.exceptions.IllegalAddressException;
import com.dma.connection.exceptions.IllegalParameterException;
import com.dma.connection.exceptions.IllegalTypeException;
import com.dma.gui.Text;

public class DeviceCommunicationHandler implements ErrorHandler<String> {
    @Override
    public String execute(Action<String> a) {
        try {
            return a.action();
        } catch (DeviceDisconnectedException e) {
            Messages.showError(e, Text.DEVICE_DOES_NOT_RESPOND);
        } catch (IllegalParameterException e) {
            Messages.showError(e, "Error in parameter: " + e.getMessage());
        } catch (IllegalAddressException | IllegalTypeException e) {
            Messages.showError(e, "Error in map description");
        } catch (Exception e) {
            Messages.showError(e, Text.SOMETHING_BAD);
        }
        return null;
    }
}
