package com.dma.gui.errors;

public class LoadLibraryHandler implements ErrorHandler<Boolean> {

    @Override
    public Boolean execute(Action<Boolean> a) {
        try {
            return a.action();
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            Messages.showError("Can't load rxtxSerial.dll");
        } catch (Exception e) {
            Messages.showError(e, "Something bad happened");
        }
        return false;
    }
}
