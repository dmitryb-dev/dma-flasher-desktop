package com.dma.gui.errors;

import com.dma.connection.exceptions.DeviceDisconnectedException;
import com.dma.gui.Text;

public class DeviceCheckHandler implements ErrorHandler<String> {
    @Override
    public String execute(Action<String> a) {
        try {
            return a.action();
        } catch (DeviceDisconnectedException e) {
            Messages.showError(e, Text.DEVICE_DOES_NOT_RESPOND);
        } catch (Exception e) {
            Messages.showError(e, Text.SOMETHING_BAD);
        }
        return null;
    }
}
