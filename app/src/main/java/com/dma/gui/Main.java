package com.dma.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        System.setProperty("java.library.path", ".");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view.fxml"));
        Parent root = loader.load();

        primaryStage.getIcons().clear();
        primaryStage.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream("icon256.png")));

        primaryStage.minWidthProperty().set(400);
        primaryStage.minHeightProperty().set(300);
        primaryStage.setTitle("DMA device customizer");
        primaryStage.setScene(new Scene(root, 800, 700));

        Controller controller = loader.getController();
        primaryStage.setOnCloseRequest(event -> { event.consume(); controller.onExit(null); });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
