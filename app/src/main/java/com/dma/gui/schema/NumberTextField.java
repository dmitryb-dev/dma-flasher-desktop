package com.dma.gui.schema;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class NumberTextField extends TextField implements ChangeListener<String> {

    private long minValue, maxValue;
    private boolean isFloat;

    public NumberTextField(long minValue, long maxValue, boolean isFloat) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.isFloat = isFloat;
        textProperty().addListener(this);
        setText("" + (isFloat? getDefault() : Integer.toString((int) getDefault())));
        focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue && !isValid(getText())) {
                Alert alert = new Alert(Alert.AlertType.ERROR,
                        "Wrong value! Allowed values: min = " + minValue + ", max = " + maxValue);
                alert.setHeaderText(null);
                setText("" + (isFloat? parsed : Integer.toString((int) parsed)));
                alert.showAndWait();
            }
        });
    }

    private boolean isCancel = false;
    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (isCancel) { isCancel = false; return; }

        if (newValue.length() == 0) return;
        if (minValue < 0 && newValue.equals("-")) return;
        if (isValid(newValue)) return;
        isCancel = true;
        setText(oldValue);
    }

    private double getDefault() {
        if (minValue < 0 && maxValue > 0) return 0;
        return minValue > 0? minValue : maxValue;
    }

    private double parsed;
    public int getInt() {
        return (int) parsed;
    }
    public double getDouble() {
        return parsed;
    }

    private boolean isValid(String value) {
        try {
            double parsed = isFloat? Double.parseDouble(value) : Integer.parseInt(value);
            if (parsed >= minValue && parsed <= maxValue) {
                this.parsed = parsed;
                return true;
            }
        } catch (Exception e) {}
        return false;
    }
}
