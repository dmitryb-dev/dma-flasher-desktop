package com.dma.gui.schema.fieldvisitors;

import com.dma.gui.schema.NumberTextField;
import com.dma.params.model.Param;
import com.dma.params.model.field.*;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class NodeValueGetter {

    private class Getter implements FieldVisitor {

        private Node node;
        public void setNode(Node node) {
            this.node = node;
        }

        @Override
        public String visit(Field field) {
            return visit(new StringField());
        }

        @Override
        public Boolean visit(BooleanField booleanField) {
            return ((CheckBox) node).selectedProperty().getValue();
        }

        @Override
        public Double visit(FloatNumberField floatNumberField) {
            return ((NumberTextField) node).getDouble();
        }

        @Override
        public Integer visit(NumberField numberField) {
            return ((NumberTextField) node).getInt();
        }

        @Override
        public Object visit(PresetField presetField) {
            ChoiceBox<String> box = (ChoiceBox<String>) node;
            int index = box.getSelectionModel().selectedIndexProperty().getValue();
            if (index < 0) return null;
            return presetField.get(index).getValue();
        }

        @Override
        public String visit(StringField stringField) {
            return ((TextField) node).getText().toString();
        }
    }
    private Getter getter = new Getter();

    public Object getValue(Node node, Param param) {
        getter.setNode(node);
        return param.getField().accept(getter);
    }

}
