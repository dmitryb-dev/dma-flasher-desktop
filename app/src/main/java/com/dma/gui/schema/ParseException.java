package com.dma.gui.schema;

import java.util.function.Supplier;

public class ParseException extends RuntimeException {

    public static <B> B tryParse(String what, String info, Supplier<B> func) {
        try {
            return func.get();
        } catch (Throwable e) {
            String message = "Can't parse " + what + (info != null? ":\n" + info : "");
            throw new ParseException(message, e);
        }
    }

    public ParseException() {

    }

    public ParseException(String message) {
        super(message);
    }

    public ParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseException(Throwable cause) {
        super(cause);
    }

    protected ParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
