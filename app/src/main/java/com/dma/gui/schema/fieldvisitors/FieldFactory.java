package com.dma.gui.schema.fieldvisitors;

import com.dma.gui.schema.NumberTextField;
import com.dma.params.model.Value;
import com.dma.params.model.field.*;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class FieldFactory {

    private FieldVisitor factory = new FieldVisitor() {

        @Override
        public CheckBox visit(BooleanField booleanField) {
            CheckBox box = new CheckBox();
            box.selectedProperty().setValue(booleanField.getDefaultValue());
            return box;
        }

        @Override
        public TextField visit(FloatNumberField floatNumberField) {
            TextField field = new NumberTextField(
                    (long) floatNumberField.getMin(),
                    (long) floatNumberField.getMax(), true);
            field.setMaxWidth(Double.MAX_VALUE);
            field.setText(floatNumberField.getDefaultValue().toString());
            return field;
        }

        @Override
        public TextField visit(NumberField numberField) {
            TextField field = new NumberTextField(numberField.getMin(), numberField.getMax(), false);
            field.setMaxWidth(Double.MAX_VALUE);
            field.setText(numberField.getDefaultValue().toString());
            return field;
        }

        @Override
        public ChoiceBox visit(PresetField presetField) {
            ChoiceBox box = new ChoiceBox();
            box.setMaxWidth(Double.MAX_VALUE);
            for (Value v : presetField)
                box.getItems().add(v.getDescription());
            box.getSelectionModel().select(presetField.getDefaultValue().intValue());
            return box;
        }

        @Override
        public TextField visit(StringField stringField) {
            TextField field = new TextField();
            field.textProperty().addListener((ov, oldValue, newValue) -> {
                if (field.getText().length() > stringField.getMaxLength()) {
                    String s = field.getText().substring(0, stringField.getMaxLength());
                    field.setText(s);
                }
            });
            field.setMinWidth(300);
            field.setMaxWidth(Double.MAX_VALUE);
            field.setText(stringField.getDefaultValue());
            return field;
        }

        @Override
        public Object visit(Field field) {
            System.out.println(field.getClass());
            return visit(new StringField());
        }
    };

    public Node getFieldFor(Field field) {
        return (Node) field.accept(factory);
    }
}
