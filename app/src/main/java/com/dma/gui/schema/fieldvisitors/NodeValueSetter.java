package com.dma.gui.schema.fieldvisitors;

import com.dma.params.model.Param;
import com.dma.params.model.field.*;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class NodeValueSetter {

    private class Setter implements FieldVisitor {

        private Node node;
        private String value;
        public void setValue(Node node, String value) {
            this.node = node;
            this.value = value;
        }

        private void setText(Node node, String text) {
            ((TextField) node).setText(text.toString());
        }

        @Override
        public Object visit(Field field) {
            setText(node, value.toString());
            return null;
        }

        @Override
        public Object visit(BooleanField booleanField) {
            ((CheckBox) node).selectedProperty().setValue(Boolean.parseBoolean(value));
            return null;
        }

        @Override
        public Object visit(FloatNumberField floatNumberField) {
            setText(node, value.toString());
            return null;
        }

        @Override
        public Object visit(NumberField numberField) {
            setText(node, value.toString());
            return null;
        }

        @Override
        public Object visit(PresetField presetField) {
            ((ChoiceBox<String>) node).getSelectionModel().select(presetField.getValueIndex(value));
            return null;
        }

        @Override
        public Object visit(StringField stringField) {
            setText(node, value.toString());
            return null;
        }
    }
    private Setter setter = new Setter();

    public Object setValue(Node node, Param param, String value) {
        setter.setValue(node, value);
        return param.getField().accept(setter);
    }

}
