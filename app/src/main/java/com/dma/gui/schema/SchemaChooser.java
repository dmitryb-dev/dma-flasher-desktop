package com.dma.gui.schema;

import com.dma.gui.LastParamsSaverLoader;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.util.prefs.Preferences;

import static com.dma.gui.LastParamsSaverLoader.getAppDirectory;
import static com.dma.gui.Text.DIRECTORY_DIALOG_TITLE;
import static com.dma.gui.Text.FILE_DIALOG_TITLE;
import static com.dma.gui.Text.SETTINGS_FILE_DIALOG_TITLE;

public class SchemaChooser {

    public SchemaChooser(Window window) {
        this.window = window;
    }

    public File getDirectoryByDialog() {
        File directory = getFileByDialog(DIRECTORY_DIALOG_TITLE, false, getLastOpenedDirectory());
        if (directory != null) {
            saveDirectory(directory);
            return directory;
        }
        return null;
    }

    private LastParamsSaverLoader loader = new LastParamsSaverLoader();

    public File getFileByDialog() {
        return getFileByDialog(FILE_DIALOG_TITLE, true, new File(loader.getMAPDirectory()));
    }
    public File getSettingsFileByDialog() {
        return getFileByDialog(SETTINGS_FILE_DIALOG_TITLE, false, new File(loader.getSETTINGSDirectory()));
    }
    public File getFileBySaveDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save schema");
        fileChooser.getExtensionFilters().addAll(JSON_FILTER);

        File initDirectory = new File(loader.getSETTINGSDirectory());
        if (!initDirectory.exists()) initDirectory = LastParamsSaverLoader.getAppDirectory(this);
        fileChooser.setInitialDirectory(initDirectory);
        return fileChooser.showSaveDialog(window);
    }
    private File getFileByDialog(String title, boolean isMap, File initDirectory) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.getExtensionFilters().addAll(
                isMap? EMAP_FILTER : JSON_FILTER,
                isMap? JSON_FILTER : EMAP_FILTER,
                ALL_SUPPOERTED,
                ALL_FILES_FILTER);
        if (!initDirectory.exists()) initDirectory = LastParamsSaverLoader.getAppDirectory(this);
        fileChooser.setInitialDirectory(initDirectory);
        return fileChooser.showOpenDialog(window);
    }

    private void saveDirectory(File f) {
        if (!f.exists() || !f.isDirectory()) {
            f = f.getParentFile();
            if (!f.exists()) return;
        }
        Preferences p = Preferences.userNodeForPackage(this.getClass());
        p.put(DIRECTORY_KEY, f.getAbsolutePath());
    }
    private File getLastOpenedDirectory() {
        Preferences p = Preferences.userNodeForPackage(this.getClass());
        String lastPath = p.get(DIRECTORY_KEY, null);
        File file = lastPath != null? new File(lastPath) : getAppDirectory(this);
        if (!file.exists() || !file.isDirectory()) {
            file = file.getParentFile();
            if (!file.exists()) file = getAppDirectory(this);
        }
        return file;
    }

    private static final String DIRECTORY_KEY = "LAST_OPENED_DIRECTORY";

    private static final FileChooser.ExtensionFilter
            ALL_SUPPOERTED = new FileChooser.ExtensionFilter("All supported", "*.dmaset", "*.emap"),
            JSON_FILTER = new FileChooser.ExtensionFilter("DMA settings", "*.dmaset"),
            EMAP_FILTER = new FileChooser.ExtensionFilter("Map", "*.emap"),
            ALL_FILES_FILTER = new FileChooser.ExtensionFilter("All files", "*.*");

    private Window window;

}
