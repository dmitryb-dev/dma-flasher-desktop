package com.dma.gui.schema;

import com.dma.gui.schema.fieldvisitors.FieldFactory;
import com.dma.gui.schema.fieldvisitors.NodeValueGetter;
import com.dma.gui.schema.fieldvisitors.NodeValueSetter;
import com.dma.params.dao.SchemaDAO;
import com.dma.params.model.Group;
import com.dma.params.model.Param;
import com.dma.params.model.Schema;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SchemaView implements SchemaDAO{

    private final TextArea descriptionView;
    private TabPane tabs;
    private Schema basicSchema;

    public SchemaView(TabPane tabs, TextArea description) {
        this.tabs = tabs;
        this.descriptionView = description;
    }

    public void show(Schema schema) {
        this.basicSchema = schema;
        tabs.getTabs().clear();
        for (Group group : schema)
            showGroup(group);
    }

    private void showGroup(Group group) {
        Tab tab = new Tab(group.getName());
        GridPane gridPane = getGridPlane();
        int row = 0;
        for (Param p : group) {
            if (p.getName() == null) {
                gridPane.add(new Label(""), 0, row);
                row++;
                continue;
            }

            Node name = getNameView(p);
            gridPane.add(name, 0, row);
            Node control = getValueView(p);
            gridPane.add(control, 1, row);
            controls.put(control, p);
            
            setDescriptionAction(descriptionView, p.getDescription(), name, control);
       //     Node description = getDescriptionView(p);
       //     if (description != null) gridPane.add(description, 2, row);
            row++;
        }
    //    HBox centerPoser = new HBox(gridPane);
   //     centerPoser.setAlignment(Pos.CENTER);
        ScrollPane scrollPane = new ScrollPane(gridPane);
    //    scrollPane.setFitToWidth(true);
   //     scrollPane.getStyleClass().add("schema");
        tab.setContent(scrollPane);
        tabs.getTabs().add(tab);
    }

    private void setDescriptionAction(TextArea destination, String text, Node... targets) {
        Arrays.stream(targets).forEach(target -> target.setOnMouseClicked(event -> destination.setText(text)));
    }
    
    private GridPane getGridPlane() {
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 10, 20));
        gridPane.setHgap(20);
        gridPane.setVgap(5);

        ColumnConstraints nameConstrain = new ColumnConstraints();
        nameConstrain.setHalignment(HPos.RIGHT);
        ColumnConstraints fieldConstrain = new ColumnConstraints();
        gridPane.getColumnConstraints().addAll(nameConstrain, fieldConstrain);

        return gridPane;
    }

    private Node getNameView(Param p) {
        Label label = new Label(p.getName() + ":");
        return label;
    }

    private Node getDescriptionView(Param p) {
        if (p.getDescription() == null) return null;
        Label label = new Label(p.getDescription());
        label.setTextFill(Color.GRAY);
        return label;
    }

    private Node getValueView(Param p) {
        return fieldFactory.getFieldFor(p.getField());
    }
    private FieldFactory fieldFactory = new FieldFactory();

    private Map<Node, Param> controls = new HashMap<>();

    private NodeValueGetter valueGetter = new NodeValueGetter();
    public Map<Param, Object> getValues() {
        Map<Param, Object> map = new HashMap<>();
        for (Map.Entry<Node, Param> pair : controls.entrySet()) {
            Param param = pair.getValue();
            Node node = pair.getKey();
            map.put(param, valueGetter.getValue(node, param));
        }
        return map;
    }

    private NodeValueSetter setter = new NodeValueSetter();
    public int setValues(Map<Param, Object> map) {
        int errors = 0;
        for (Map.Entry<Param, Object> pair : map.entrySet()) {
            try {
                setParam(pair.getKey(), (String) pair.getValue());
            } catch (Exception e) {
                errors++;
            }
        }
        return errors;
    }

    public void setParam(Param p, String value) {
        Node control = null;
        for (Map.Entry<Node, Param> pair : controls.entrySet())
            if (pair.getValue().equals(p)) {
                control = pair.getKey();
                break;
            }

        if (control != null) setter.setValue(control, p, value);
    }


    @Override
    public Schema getSchema() throws Exception {
        Schema schema = basicSchema;
        Map<Param, Object> read = getValues();
        for (Group g : schema)
            for (Param p : g) {
                if (p.getField() != null)
                    p.getField().setDefaultValue(read.get(p));
            }
        return schema;
    }
}
