package com.dma.gui;

import gnu.io.CommPortIdentifier;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class COMDeviceUtils {

    public static List<CommPortIdentifier> getPortsList() {
        List<CommPortIdentifier> ports = new ArrayList<>();
        Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();
        while (portEnum.hasMoreElements()) {
            CommPortIdentifier portIdentifier = portEnum.nextElement();
            if (portIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL)
                ports.add(portIdentifier);
        }
        return ports;
    }

}
