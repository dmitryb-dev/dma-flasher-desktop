package com.dma.gui;

import com.dma.connection.Logs.ConnectionLogger;
import com.dma.connection.Logs.LogsDestination;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;

public class LogWindow implements LogsDestination{

    @FXML
    public TextArea logsArea;

    public LogWindow() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/logger.fxml"));
        loader.setController(this);
        Parent root = loader.load();
        Stage stage = new Stage();
        stage.minWidthProperty().set(400);
        stage.minHeightProperty().set(300);
        stage.setTitle("Connection logger");
        stage.setScene(new Scene(root, 400, 300));

        stage.setOnCloseRequest(event -> { disconnect(); });
        ConnectionLogger.addLogger(this);
        stage.show();
    }

    private void disconnect() {
        ConnectionLogger.removeLogger(this);
    }

    @Override
    public void onLogReceived(String log) {
        logsArea.setText(logsArea.getText() + log + "\n");
    }
}
