package com.dma.connection;

import com.dma.connection.exceptions.DeviceException;
import com.dma.params.model.Param;

import java.io.IOException;

public interface Protocol {

    String send(String request) throws IOException;
    String getDeviceVersion() throws DeviceException;
    String getDeviceName() throws DeviceException;
    String read(Param param) throws DeviceException;
    void write(Param param, Object value) throws DeviceException;

}
