package com.dma.connection.exceptions;

import java.io.IOException;

public class DeviceDisconnectedException extends DeviceException {

    public DeviceDisconnectedException() {
    }

    public DeviceDisconnectedException(IOException e) {
        super("IOException", e);
    }
}
