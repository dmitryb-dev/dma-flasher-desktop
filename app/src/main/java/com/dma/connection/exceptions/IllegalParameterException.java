package com.dma.connection.exceptions;

import com.dma.params.model.Param;

public class IllegalParameterException extends DeviceException {

    public IllegalParameterException() {
        super("Unknown parameter");
    }

    public IllegalParameterException(Param p) {
        super(p.getName());
    }

}
