package com.dma.connection;

import java.io.IOException;

public interface Connection {

    byte[] read(int lines) throws IOException;
    void write(byte[] value) throws IOException;

}
