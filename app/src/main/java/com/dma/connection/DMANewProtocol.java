package com.dma.connection;

import com.dma.connection.Logs.ConnectionLogger;
import com.dma.connection.exceptions.*;
import com.dma.params.model.Param;


import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DMANewProtocol implements Protocol {

    private Connection connection;

    public DMANewProtocol( Connection connection) {
        this.connection = connection;
    }


    @Override
    public String getDeviceVersion() throws DeviceException {
        try {
            return send("ATI1", 3).split("(\n|\r)")[0];
        } catch (IOException e) {
            throw new DeviceDisconnectedException(e);
        }
    }

    @Override
    public String getDeviceName() throws DeviceException {
        try {
            return send("ATI", 3).split("(\n|\r)")[0];
        } catch (IOException e) {
            throw new DeviceDisconnectedException(e);
        }
    }

    @Override
    public String read(Param param) throws DeviceException {
        try {
            ConnectionLogger.log("-------------------------------");
            String answer = send(createReadCommand(param));
            ConnectionLogger.log("ANSWER = " + answer);
            if (answer.indexOf(ADDRESS_ERROR) >= 0) throw new IllegalAddressException();
            if (answer.indexOf(TYPE_ERROR) >= 0) throw new IllegalTypeException();
            return answer;
        } catch (IOException e) {
            throw new DeviceDisconnectedException(e);
        }
    }

    @Override
    public void write(Param param, Object value) throws DeviceException {
        if (value == null) throw new IllegalParameterException(param);
        try {
            ConnectionLogger.log("-------------------------------");
            String answer = send(createWriteCommand(param, value.toString()));
            ConnectionLogger.log("ANSWER = " + answer);
            if (answer.indexOf(ADDRESS_ERROR) >= 0) throw new IllegalAddressException();
            if (answer.indexOf(TYPE_ERROR) >= 0) throw new IllegalTypeException();
            if (answer.indexOf(PARAMETER_ERROR) >= 0) throw new IllegalParameterException(param);
        } catch (IOException e) {
            throw new DeviceDisconnectedException(e);
        }
    }

    public String send(String request, int lines) throws IOException {
        byte[] command = getCommandFromString(request);
        connection.write(command);
        return new String(readWithoutEcho(command, lines), StandardCharsets.US_ASCII);
    }
    @Override
    public String send(String request) throws IOException {
        return send(request, 2);
    }

    private byte[] getCommandFromString(String string) {
        ByteArray bytes = new ByteArray();
        bytes.add(string.getBytes(StandardCharsets.US_ASCII));
        bytes.add((byte) 0x0D, (byte) 0x0A);
        return bytes.get();
    }

    private byte[] readWithoutEcho(byte[] writeCommand, int lines) throws IOException {
        byte[] data = connection.read(lines);
        return ByteArray.sub(data, writeCommand.length);
    }

    public String createWriteCommand(Param param, String value) {
        return "AT+WR=" + param.getAddress() + "," + param.getType() + "," + value.toString();
    }
    public String createReadCommand(Param param) {
        return "AT+RD=" + param.getAddress() + "," + param.getType();
    }

    private static final String
            ADDRESS_ERROR = "#ERR ADRES",
            TYPE_ERROR = "#ERR TYPE",
            PARAMETER_ERROR = "#ERR PARAMETR";

}
