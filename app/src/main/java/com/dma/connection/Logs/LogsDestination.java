package com.dma.connection.Logs;

public interface LogsDestination {

    void onLogReceived(String log);

}
