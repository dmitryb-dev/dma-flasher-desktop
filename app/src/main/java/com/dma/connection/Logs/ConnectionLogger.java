package com.dma.connection.Logs;

import java.util.ArrayList;
import java.util.Collection;

public class ConnectionLogger {

    private static boolean isEnabled;
    public static void enable(boolean isEnabled) {
        ConnectionLogger.isEnabled = isEnabled;
    }

    private static Collection<LogsDestination> listeners = new ArrayList<>();

    public static void addLogger(LogsDestination logger) {
        listeners.add(logger);
    }
    public static void removeLogger(LogsDestination logger) {
        listeners.remove(logger);
    }

    public static void log(String s) {
        if (isEnabled) {
            for (LogsDestination logger : listeners)
                logger.onLogReceived(s);
        }
    }

}
