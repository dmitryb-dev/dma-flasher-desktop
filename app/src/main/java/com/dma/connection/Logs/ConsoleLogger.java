package com.dma.connection.Logs;

public class ConsoleLogger implements LogsDestination {
    @Override
    public void onLogReceived(String log) {
        System.out.println(log);
    }
}
