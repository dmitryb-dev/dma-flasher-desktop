package com.dma.connection;

import java.nio.charset.StandardCharsets;

public class ProtocolDebugConnection implements Connection {

    @Override
    public byte[] read(int lines) {
        return "10".getBytes(StandardCharsets.US_ASCII);
    }

    @Override
    public void write(byte[] value) {
        System.out.println("Written: " + new String(value, StandardCharsets.US_ASCII));
    }
}
