package com.dma.connection;

import java.util.ArrayList;
import java.util.Collection;

public class ByteArray {

    private Collection<byte[]> arrays = new ArrayList<>();
    private int summaryLength;

    public void add(byte... array) {
        arrays.add(array);
        summaryLength += array.length;
    }

    public byte[] get() {
        return mergeAll(arrays, summaryLength);
    }

    private byte[] mergeAll(Collection<byte[]> source, int summaryLength) {
        byte[] res = new byte[summaryLength];
        int startIndex = 0;
        for (byte[] array : source) {
            int endIndex = startIndex + array.length;
            for (int i = startIndex, j = 0; i < endIndex; i++, j++)
                res[i] = array[j];
            startIndex = endIndex;
        }
        return res;
    }

    public static byte[] sub(byte[] source, int start) {
        byte[] res = new byte[source.length - start];
        for (int i = 0; i < res.length; i++)
            res[i] = source[i + start];
        return res;
    }

}
