package com.dma.connection;

import com.dma.connection.Logs.ConnectionLogger;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class COMConnection implements Connection {

    private int speed;
    private SerialPort port;
    private CommPortIdentifier identifier;
    public COMConnection(CommPortIdentifier identifier, int speed) throws Exception {
        if (identifier.isCurrentlyOwned()) throw new IOException("Port already in use");
        CommPort port = identifier.open(this.getClass().getName(), 2000);
        if (port instanceof SerialPort) {
            this.speed = speed;
            this.identifier = identifier;
            SerialPort serialPort = this.port = (SerialPort) port;
            serialPort.enableReceiveTimeout(1000);
            serialPort.setInputBufferSize(1024);
            serialPort.setSerialPortParams(speed, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            out = serialPort.getOutputStream();
        } else {
            throw new IOException("Unsupported port type");
        }
    }

    public int getSpeed() {
        return speed;
    }

    public CommPortIdentifier getIdentifier() {
        return identifier;
    }

    public void close() {
        try {
            out.close();
            port.close();
        } catch (Exception e) {}
    }

    @Override
    public byte[] read(int lines) throws IOException {
        try {
            ConnectionLogger.log("RESPONSE: ");
            in = port.getInputStream();
            ReadThread readThread = new ReadThread(lines);
            readThread.start();
            readThread.join();
            if (readThread.getError() != null) throw new IOException("Can't read data", readThread.getError());
            ConnectionLogger.log(new String( readThread.getReadData(), StandardCharsets.US_ASCII));
            return readThread.getReadData();
        } catch (InterruptedException e) {
            throw new IOException("Thread was stopped");
        } finally {
            in.close();
        }

    }

    @Override
    public void write(byte[] value) throws IOException {
        ConnectionLogger.log("REQUEST:\n" + new String(value, StandardCharsets.US_ASCII));
        out.write(value);
        out.flush();
    }
/*
    private class ReadThread extends Thread {
        private static final int BUFFER_SIZE = 1024;
        private byte[] buffer = new byte[BUFFER_SIZE];
        private List<Byte> readData = new ArrayList<Byte>();

        @Override
        public void run() {
            int read;
            try {
                while ((read = in.read(buffer)) != -1) {
                    System.out.println(read);
                    for (int i = 0; i < read; i++) readData.add(buffer[i]);
                }
            } catch (IOException e) {
                error = e;
                return;
            }
        }

        public byte[] getReadData() {
            byte[] result = new byte[readData.size()];
            IntStream.range(0, readData.size()).forEach(i -> result[i] = readData.get(i));
            return result;
        }

        public Exception getError() {
            return error;
        }
        private Exception error;
    }*/

    private class ReadThread extends Thread {

        private static final int BUFFER_SIZE = 1024;
        private byte[] readData = new byte[BUFFER_SIZE];
        private int readLength;
        private Exception error;
        private int lines;
        public ReadThread(int lines) { this.lines = lines; }

        @Override
        public void run() {
            long lastSuccessfulReadTime = System.currentTimeMillis();
            while (true) {
                try {
                    int toRead = in.available();
                    if (toRead > 0) {
                        in.read(readData, readLength, toRead);
                   //     System.out.println(javax.xml.bind.DatatypeConverter.printHexBinary(readData));
                        readLength += toRead;
                        if (hasEnds(readData, lines, readLength)) {
                            readLength -= 2;
                            return;
                        }
                        lastSuccessfulReadTime = System.currentTimeMillis();
                    }
                    else
                        if (elapsedTime(lastSuccessfulReadTime) > TIMEOUT) {
                            error = new IOException("Timeout");
                            return;
                        }
                } catch (Exception e) {
                    error = e;
                    return;
                }
            }
        }

        public byte[] getReadData() {
            byte[] data = new byte[readLength];
            for (int i = 0; i < data.length; i++) {
                data[i] = readData[i];
            }
            return data;
        }

        private int indexOfEnd(byte[] array, int start, int length) {
            int indexOfEnd = -1;
            for (int i = start; i < length; i++)
                if (array[i] == 0x0A) {
                    indexOfEnd = i;
                    break;
                }
            if (indexOfEnd > 0 && array[indexOfEnd - 1] == 0x0D)
                return indexOfEnd - 1;
            return -1;
        }
        private boolean hasEnds(byte[] array, int times, int length) {
            int indexOfEnd = -1;
            for (int i = 0; i < times; i++)
                if ((indexOfEnd = indexOfEnd(array, indexOfEnd + 2, length)) < 0) return false;
            return true;
        }

        private long elapsedTime(long from) {
            return System.currentTimeMillis() - from;
        }

        public Exception getError() {
            return error;
        }

    };

    private InputStream in;
    private OutputStream out;

    private static final int TIMEOUT = 4000;
}
