package com.dma.creatorgui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/creator.fxml"));
        Parent root = loader.load();
        primaryStage.minWidthProperty().set(400);
        primaryStage.minHeightProperty().set(300);
        primaryStage.setTitle("DMA schema creator");
        primaryStage.setScene(new Scene(root, 1200, 700));

        Controller controller = loader.getController();
        primaryStage.setOnCloseRequest(event -> { event.consume(); controller.onExit(null); });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
