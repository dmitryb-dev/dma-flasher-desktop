package com.dma.creatorgui;

import com.dma.connection.exceptions.DeviceException;
import com.dma.params.model.Group;
import com.dma.params.model.Param;
import com.dma.params.model.Schema;
import com.dma.params.model.Value;
import com.dma.params.model.field.*;
import com.dma.params.utils.SchemaCreatorJSON;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Controller {

    @FXML
    public TextField idField;
    @FXML
    public TextField fromId;
    @FXML
    public TextField maxS;
    private com.dma.gui.Controller baseController;

    @FXML
    TabPane tabs;
    @FXML
    Menu schemasMenu;
    @FXML
    TitledPane title;

    @FXML
    protected void initialize() {
        baseController = new com.dma.gui.Controller(tabs, schemasMenu, title);
        schema = new Schema("PLEASE, ENTER YOUR DEVICE DESCRIPTION");
        invalidate();
    }

    public void onExit(ActionEvent actionEvent) {
        baseController.onExit(actionEvent);
    }

    public void showSchema(Schema schema, String defaultName) {
        schema.setDevice(deviceName.getText());
        schema.setDescription(schemaDescription.getText().equals("")? null : schemaDescription.getText());
        int deviceSpeed = speed.getText().equals("")? 115200 : Integer.parseInt(speed.getText());
        schema.setSpeed(deviceSpeed);
        baseController.showSchema(schema, defaultName);
    }

    Schema schema;

    public void onOpen(ActionEvent actionEvent) throws DeviceException {
        baseController.onOpen(actionEvent);
        schema = baseController.getOpenedSchema();
    }

    public void onLoad(ActionEvent actionEvent) {
        baseController.onLoad(actionEvent);
    }

    public void onSave(ActionEvent actionEvent) {
        baseController.onSave(actionEvent);
    }

    private LinkedList<Schema> history = new LinkedList<>();

    public void invalidate() {
        history.add(schema.clone());
        int selectedTab = tabs.getSelectionModel().getSelectedIndex();
        showSchema(schema, "__EDITOR");
        tabs.getSelectionModel().select(selectedTab);
    }

    @FXML
    public TextField speed;
    @FXML
    public TextField schemaDescription;
    @FXML
    public TextField deviceName;
    @FXML
    public TextField name;
    @FXML
    public TextField address;
    @FXML
    public TextField type;
    @FXML
    public TextField description;
    private Param createParam(Field f) {
        if (f instanceof FieldWithId) {
            ((FieldWithId) f).setId(idField.getText().equals("")? null : idField.getText());;
        }
        return new Param(
                name.getText(),
                address.getText(),
                type.getText(),
                f,
                description.getText().equals("")? null : description.getText()
        );
    }

    @FXML
    TextField minN;
    @FXML
    TextField maxN;
    public void createNumber(ActionEvent actionEvent) {
        NumberField f;
        try {
            f = new NumberField(Long.parseLong(minN.getText()), Long.parseLong(maxN.getText()));
        } catch (Exception e) {
            f = new NumberField();
        }
        schema.getGroup(tabs.getSelectionModel().getSelectedIndex()).addParam(createParam(f));
        invalidate();
    }

    @FXML
    TextField minF;
    @FXML
    TextField maxF;
    public void createFloat(ActionEvent actionEvent) {
        FloatNumberField f;
        try {
            f = new FloatNumberField(Double.parseDouble(minF.getText()), Double.parseDouble(maxF.getText()));
        } catch (Exception e) {
            f = new FloatNumberField();
        }

        schema.getGroup(tabs.getSelectionModel().getSelectedIndex()).addParam(createParam(f));
        invalidate();
    }

    public void createBoolean(ActionEvent actionEvent) {
        schema.getGroup(tabs.getSelectionModel().getSelectedIndex()).addParam(createParam(new BooleanField()));
        invalidate();
    }

    public void createString(ActionEvent actionEvent) {
        StringField s;
        try {
            s = new StringField("", Integer.parseInt(maxS.getText()));
        } catch (Exception e) {
            s = new StringField();
        }
        schema.getGroup(tabs.getSelectionModel().getSelectedIndex()).addParam(createParam(s));
        invalidate();
    }

    @FXML
    TextArea preset;
    public void createPreset(ActionEvent actionEvent) {
        PresetField presetField = new PresetField();
        Values values = new Values(preset.getText());
        Iterator<String> iterator = values.iterator();
        while (iterator.hasNext()) {
            String value = iterator.next();
            String description = iterator.next();
            presetField.addValue(new Value(description, value));
        }
  //      presetField.setId(idField.getText().equals("")? null : idField.getText());
        schema.getGroup(tabs.getSelectionModel().getSelectedIndex()).addParam(createParam(presetField));
        invalidate();
    }

    @FXML
    public TextField tabName;

    public void createTab(ActionEvent actionEvent) {
        schema.addGroup(new Group(tabName.getText()));
        invalidate();
    }

    public void onUndo(ActionEvent actionEvent) {
        if (history.size() > 0) {
            history.removeLast();
            schema = history.getLast();
            history.removeLast();
            invalidate();
        }
    }

    public void createSpace(ActionEvent actionEvent) {
        schema.getGroup(tabs.getSelectionModel().getSelectedIndex()).addParam(new Param());
        invalidate();
    }

    public void createFromId(ActionEvent actionEvent) {
        Field f = schema.getFieldById(fromId.getText());
        if (f == null) return;
        schema.getGroup(tabs.getSelectionModel().getSelectedIndex()).addParam(createParam(f));
        invalidate();
    }

    private class Values implements Iterable<String>{

        private List<String> parsed = new ArrayList<>();

        public Values(String source) {
            int startIndex = -1;
            while (true) {
                int endIndex = source.indexOf(';', startIndex + 1);
                if (endIndex < 0) return;
                String res = source.substring(startIndex + 1, endIndex).trim();
                parsed.add(res);
                startIndex = endIndex;
            }
        }

        @Override
        public Iterator<String> iterator() {
            return parsed.iterator();
        }
    }

}